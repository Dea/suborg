use clap::Parser;
use glob::glob;
use pretty_env_logger;
use std::fs;
use std::path::PathBuf;
use std::time::Instant;

#[macro_use]
extern crate log;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    directory: Option<String>,
}

fn copy_sub(
    pwd: PathBuf,
    path: PathBuf,
    start: Instant,
) -> Result<bool, Box<dyn std::error::Error>> {
    debug!("Current operating path: {:?}", path.display());
    let origin = path.clone();

    let mut file_to_copy = origin.clone();
    let new_name: PathBuf;
    if origin.is_dir() {
        let mut files = fs::read_dir(path)?;
        let file = files.find(|name| {
            name.as_ref()
                .unwrap()
                .path()
                .to_str()
                .unwrap()
                .ends_with("English.srt")
        });
        let mut new_path = if file.is_none() {
            return Ok(false);
        } else {
            file.unwrap().unwrap().path()
        };
        file_to_copy = new_path.clone();
        new_path.pop();
        new_name = PathBuf::from(new_path.iter().last().unwrap());
    } else {
        new_name = PathBuf::from(
            origin
                .parent()
                .unwrap()
                .parent()
                .unwrap()
                .iter()
                .last()
                .unwrap(),
        );
    };

    debug!("Name to assign: {}", new_name.to_str().unwrap());
    let mut dest = pwd.clone();
    dest.push(format!("{}.srt", new_name.to_str().unwrap()));
    debug!("Copy destination: {:?}", dest);
    if let Err(_) = fs::copy(&file_to_copy, dest.to_str().unwrap()) {
        warn!("Failed to copy.");
        return Ok(false);
    };
    info!(
        "Copied {:?} to {:?} in {}.{:0>3}ms.",
        &file_to_copy.iter().last().unwrap(),
        dest.iter().last().unwrap(),
        start.elapsed().as_micros() / 1000,
        start.elapsed().as_micros() % 1000,
    );
    return Ok(true);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    };
    pretty_env_logger::init_timed();
    let args = Args::parse();
    let pwd = if args.directory.is_none() {
        std::env::current_dir()?
    } else {
        PathBuf::from(args.directory.unwrap())
    };
    info!("Starting at {:?}", pwd);

    let mut p = pwd.clone();
    p.push("Subs/*");
    info!("Looking in {:?}", p);
    let mut count = 0;
    let start = Instant::now();
    for entry in glob(p.to_str().unwrap()).expect("Failed to read") {
        let start = Instant::now();
        let path = if entry.is_err() {
            warn!("Couldn't handle a file.");
            continue;
        } else {
            entry?
        };

        if path.is_file() {
            if !path.to_str().unwrap().ends_with("English.srt") {
                continue;
            };
            info!("No directories, starting copy.");
            copy_sub(pwd.clone(), path.clone(), start)?;
            count += 1;
            break;
        };

        info!("Starting copy.");
        let is_copied: bool = copy_sub(pwd.clone(), path.clone(), start)?;
        if !is_copied {
            continue;
        }

        count += 1;
    }
    info!(
        "Copied {} files in {}.{:0>3}ms.",
        count,
        start.elapsed().as_micros() / 1000,
        start.elapsed().as_micros() % 1000,
    );
    Ok(())
}
